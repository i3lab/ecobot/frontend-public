# Ecobot frontend

## Prerequisites

This project requires NodeJS and NPM.
[Node](http://nodejs.org/) and [NPM](https://npmjs.org/) are really easy to install.
To make sure you have them available on your machine,
try running the following command.

```sh
npm -v && node -v
6.4.1
v8.16.0
```

## Installation
Clone the repo on your local machine:
```sh
git clone https://gitlab.com/i3lab/ecobot/frontend.git
cd frontend
```
To install and set up the library, run:

```sh
npm install
```

## Usage
_Note:_ A running backend is required. See https://gitlab.com/i3lab/ecobot/server

### Running the app
```sh
npm start
```
